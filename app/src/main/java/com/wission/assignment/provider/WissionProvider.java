package com.wission.assignment.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class WissionProvider extends ContentProvider {

    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private WissionDbHelper mOpenHelper;

    static final int USER = 50;
    static final int FEED = 100;
    static final int LIKE = 150;
    static final int COMMENT = 200;

    static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = WissionContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, WissionContract.PATH_USER, USER);
        matcher.addURI(authority, WissionContract.PATH_FEED, FEED);
        matcher.addURI(authority, WissionContract.PATH_LIKE, LIKE);
        matcher.addURI(authority, WissionContract.PATH_COMMENT, COMMENT);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new WissionDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor retCursor;

        switch (sUriMatcher.match(uri)) {
            case USER:
                retCursor = mOpenHelper.getReadableDatabase().query(
                        WissionContract.UserEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case FEED:
                retCursor = mOpenHelper.getReadableDatabase().query(
                        WissionContract.FeedEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case LIKE:
                retCursor = mOpenHelper.getReadableDatabase().query(
                        WissionContract.LikeEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case COMMENT:
                retCursor = mOpenHelper.getReadableDatabase().query(
                        WissionContract.CommentEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case USER:
                return WissionContract.UserEntry.CONTENT_TYPE;
            case FEED:
                return WissionContract.FeedEntry.CONTENT_TYPE;
            case LIKE:
                return WissionContract.LikeEntry.CONTENT_TYPE;
            case COMMENT:
                return WissionContract.CommentEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;
        switch (match) {
            case USER: {
                long _id = db.insert(WissionContract.UserEntry.TABLE_NAME, null, contentValues);
                if (_id > 0) {
                    returnUri = WissionContract.UserEntry.buildUserUri(_id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            case FEED: {
                long _id = db.insert(WissionContract.FeedEntry.TABLE_NAME, null, contentValues);
                if (_id > 0) {
                    returnUri = WissionContract.FeedEntry.buildFeedUri(_id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            case LIKE: {
                long _id = db.insert(WissionContract.LikeEntry.TABLE_NAME, null, contentValues);
                if (_id > 0) {
                    returnUri = WissionContract.LikeEntry.buildLikeUri(_id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            case COMMENT: {
                long _id = db.insert(WissionContract.CommentEntry.TABLE_NAME, null, contentValues);
                if (_id > 0) {
                    returnUri = WissionContract.CommentEntry.buildCommentUri(_id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        if (selection == null) selection = "1";
        int rowsDeleted;
        switch (match) {
            case USER: {
                rowsDeleted = db.delete(WissionContract.UserEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case FEED: {
                rowsDeleted = db.delete(WissionContract.FeedEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case LIKE: {
                rowsDeleted = db.delete(WissionContract.LikeEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case COMMENT: {
                rowsDeleted = db.delete(WissionContract.CommentEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri:" + uri);

        }
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        int rowsUpdated;
        switch (match) {
            case USER: {
                rowsUpdated = db.update(WissionContract.UserEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            case FEED: {
                rowsUpdated = db.update(WissionContract.FeedEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            case LIKE: {
                rowsUpdated = db.update(WissionContract.LikeEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            case COMMENT: {
                rowsUpdated = db.update(WissionContract.CommentEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri:" + uri);

        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }
}
