package com.wission.assignment.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class WissionDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "wission.db";

    public WissionDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_USER_TABLE = "CREATE TABLE " + WissionContract.UserEntry.TABLE_NAME + " (" +
                WissionContract.UserEntry._ID + " INTEGER PRIMARY KEY," +
                WissionContract.UserEntry.COLUMN_EMAIL + " TEXT NOT NULL," +
                WissionContract.UserEntry.COLUMN_NAME + " TEXT NOT NULL," +
                WissionContract.UserEntry.COLUMN_HANDLE + " TEXT NOT NULL," +
                WissionContract.UserEntry.COLUMN_IS_MALE + " INTEGER NOT NULL," +
                WissionContract.UserEntry.COLUMN_IS_LOGGED_IN + " INTEGER NOT NULL," +
                WissionContract.UserEntry.COLUMN_DOB + " TEXT NOT NULL," +
                WissionContract.UserEntry.COLUMN_IMAGE_PATH + " TEXT NOT NULL," +
                " UNIQUE (" + WissionContract.UserEntry.COLUMN_EMAIL + ") ON CONFLICT IGNORE);";

        final String SQL_CREATE_FEED_TABLE = "CREATE TABLE " + WissionContract.FeedEntry.TABLE_NAME + " (" +
                WissionContract.FeedEntry._ID + " INTEGER PRIMARY KEY," +
                WissionContract.FeedEntry.COLUMN_VIDEO_ID + " TEXT NOT NULL," +
                WissionContract.FeedEntry.COLUMN_TITLE + " TEXT NOT NULL," +
                WissionContract.FeedEntry.COLUMN_THUMBNAIL + " TEXT NOT NULL," +
                WissionContract.FeedEntry.COLUMN_VIEWS + " INTEGER ," +
                " UNIQUE (" + WissionContract.FeedEntry.COLUMN_VIDEO_ID + ") ON CONFLICT IGNORE);";

        final String SQL_CREATE_LIKE_TABLE = "CREATE TABLE " + WissionContract.LikeEntry.TABLE_NAME + " (" +
                WissionContract.LikeEntry._ID + " INTEGER PRIMARY KEY," +
                WissionContract.LikeEntry.COLUMN_VIDEO_ID + " TEXT NOT NULL," +
                WissionContract.LikeEntry.COLUMN_USER_ID + " INTEGER NOT NULL);";

        final String SQL_CREATE_COMMENT_TABLE = "CREATE TABLE " + WissionContract.CommentEntry.TABLE_NAME + " (" +
                WissionContract.CommentEntry._ID + " INTEGER PRIMARY KEY," +
                WissionContract.CommentEntry.COLUMN_VIDEO_ID + " TEXT NOT NULL," +
                WissionContract.CommentEntry.COLUMN_USER_ID + " INTEGER NOT NULL," +
                WissionContract.CommentEntry.COLUMN_COMMENT + " TEXT );";

        db.execSQL(SQL_CREATE_USER_TABLE);
        db.execSQL(SQL_CREATE_FEED_TABLE);
        db.execSQL(SQL_CREATE_LIKE_TABLE);
        db.execSQL(SQL_CREATE_COMMENT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
