package com.wission.assignment.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class WissionContract {
    public static final String CONTENT_AUTHORITY = "com.wission.assignment";
    static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_USER = "user";
    public static final String PATH_FEED = "feed";
    public static final String PATH_LIKE = "like";
    public static final String PATH_COMMENT = "comment";

    public static final class UserEntry implements BaseColumns {

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_USER;

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER).build();

        public static final String TABLE_NAME = "user";
        public static final String COLUMN_EMAIL = "user_email";
        public static final String COLUMN_NAME = "user_name";
        public static final String COLUMN_HANDLE= "user_handle";
        public static final String COLUMN_IS_MALE = "user_is_male";
        public static final String COLUMN_IS_LOGGED_IN = "user_is_logged_in";
        public static final String COLUMN_DOB = "user_dob";
        public static final String COLUMN_IMAGE_PATH= "user_image_path";

        public static Uri buildUserUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class FeedEntry implements BaseColumns {

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FEED;

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_FEED).build();

        public static final String TABLE_NAME = "feed";
        public static final String COLUMN_VIDEO_ID = "video_id";
        public static final String COLUMN_THUMBNAIL = "video_thumbnail";
        public static final String COLUMN_VIEWS= "video_views";
        public static final String COLUMN_TITLE = "video_title";

        public static Uri buildFeedUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class LikeEntry implements BaseColumns {

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LIKE;

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_LIKE).build();

        public static final String TABLE_NAME = "like";
        public static final String COLUMN_VIDEO_ID = "video_id";
        public static final String COLUMN_USER_ID = "user_id";

        public static Uri buildLikeUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class CommentEntry implements BaseColumns {

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_COMMENT;

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_COMMENT).build();

        public static final String TABLE_NAME = "comment";
        public static final String COLUMN_VIDEO_ID = "video_id";
        public static final String COLUMN_USER_ID = "user_id";
        public static final String COLUMN_COMMENT = "comment";

        public static Uri buildCommentUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
