package com.wission.assignment.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.wission.assignment.R;
import com.wission.assignment.provider.WissionContract;
import com.wission.assignment.util.SharedPrefUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FeedIntentService extends IntentService {

    public FeedIntentService() {
        super("FeedIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            OkHttpClient client = new OkHttpClient();
            HttpUrl.Builder urlBuilder = HttpUrl.parse(getString(R.string.google_api_search_endpoint))
                    .newBuilder();

            urlBuilder.addQueryParameter("key", getString(R.string.google_api_key_wisson));
            urlBuilder.addQueryParameter("part", "snippet");
            urlBuilder.addQueryParameter("fields", "items(id/videoId,snippet(thumbnails/high/url,title)),nextPageToken,pageInfo,prevPageToken");
            urlBuilder.addQueryParameter("maxResults", "10");
            urlBuilder.addQueryParameter("order", "viewCount");
            urlBuilder.addQueryParameter("type", "video");
            if (SharedPrefUtil.getNextPageToken(this) != null)
                urlBuilder.addQueryParameter("pageToken", SharedPrefUtil.getNextPageToken(this));
            urlBuilder.addQueryParameter("q", "stand up comedy");

            String url = urlBuilder.build().toString();
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            String result = response.body().string();
            response.close();

            JSONObject resultObject = new JSONObject(result);
            if (resultObject.has("nextPageToken"))
                SharedPrefUtil.saveNextPageToken(this, resultObject.getString("nextPageToken"));
            else SharedPrefUtil.is_feed_last = true;
            JSONArray resultArray = resultObject.getJSONArray("items");
            for (int i = 0; i < resultArray.length(); i++) {
                JSONObject item = resultArray.getJSONObject(i);
                ContentValues contentValues = new ContentValues();
                contentValues.put(WissionContract.FeedEntry.COLUMN_VIDEO_ID, item.getJSONObject("id").getString("videoId"));
                contentValues.put(WissionContract.FeedEntry.COLUMN_TITLE, item.getJSONObject("snippet").getString("title"));
                contentValues.put(WissionContract.FeedEntry.COLUMN_THUMBNAIL, item.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("high").getString("url"));
                getContentResolver().insert(WissionContract.FeedEntry.CONTENT_URI, contentValues);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
