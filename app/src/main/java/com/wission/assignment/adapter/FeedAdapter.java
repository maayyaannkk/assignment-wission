package com.wission.assignment.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wission.assignment.R;
import com.wission.assignment.provider.WissionContract;

public class FeedAdapter extends RecyclerviewCursorAdapter<RecyclerView.ViewHolder> implements View.OnClickListener{

    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;

    public FeedAdapter(final Context context) {
        super();
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setOnItemClickListener(final FeedAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.list_item_feed, parent, false);
        view.setOnClickListener(this);
        return new FeedViewHolder(view);
    }

    @Override
    public void onClick(View view) {
        if (this.onItemClickListener != null) {
            final RecyclerView recyclerView = (RecyclerView) view.getParent();
            final int position = recyclerView.getChildLayoutPosition(view);
            if (position != RecyclerView.NO_POSITION) {
                final Cursor cursor = this.getItem(position);
                this.onItemClickListener.onItemClicked(cursor);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, Cursor cursor) {
        if (holder instanceof FeedViewHolder) {
            ((FeedViewHolder) holder).bindData(cursor);
        }
    }

    private static class FeedViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewVideo;
        TextView textViewTitle;
        Context context;

        FeedViewHolder(final View itemView) {
            super(itemView);
            context = itemView.getContext();
            imageViewVideo = itemView.findViewById(R.id.imageViewVideo);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
        }

        void bindData(final Cursor cursor) {
            Glide.with(context)
                    .load(cursor.getString(cursor.getColumnIndex(WissionContract.FeedEntry.COLUMN_THUMBNAIL)))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(imageViewVideo);

            textViewTitle.setText(cursor.getString(cursor.getColumnIndex(WissionContract.FeedEntry.COLUMN_TITLE)));
        }
    }
    public interface OnItemClickListener {
        void onItemClicked(Cursor cursor);
    }
}
