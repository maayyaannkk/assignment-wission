package com.wission.assignment.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wission.assignment.R;
import com.wission.assignment.bean.Comment;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {
    private List<Comment> commentList;

    public CommentAdapter(List<Comment> commentList) {
        this.commentList = commentList;
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_comment, parent, false);
        return new CommentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder holder, int position) {
        Comment comment = commentList.get(position);
        Bitmap selectedImage = BitmapFactory.decodeFile(comment.getImagePath());
        holder.imageViewUser.setImageBitmap(selectedImage);
        holder.textViewComment.setText(comment.getComment());
        holder.textViewUserName.setText(comment.getUserName());
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    class CommentViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imageViewUser;
        TextView textViewUserName, textViewComment;
        Context context;

        CommentViewHolder(final View itemView) {
            super(itemView);
            context = itemView.getContext();
            imageViewUser = itemView.findViewById(R.id.imageViewUser);
            textViewUserName = itemView.findViewById(R.id.textViewUserName);
            textViewComment = itemView.findViewById(R.id.textViewComment);
        }
    }
}
