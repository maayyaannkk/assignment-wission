package com.wission.assignment.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPrefUtil {

    public static boolean is_feed_last = false;

    public static void saveNextPageToken(Context context, String token) {
        SharedPreferences.Editor editor = context.getSharedPreferences("WISSION_SHARED_PREF", MODE_PRIVATE).edit();
        editor.putString("WISSION_SHARED_PREF_NEXT_TOKEN", token);
        editor.apply();
    }

    public static String getNextPageToken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("WISSION_SHARED_PREF", MODE_PRIVATE);
        return prefs.getString("WISSION_SHARED_PREF_NEXT_TOKEN", null);
    }

    public static void saveLastPosition(Context context, int position) {
        SharedPreferences.Editor editor = context.getSharedPreferences("WISSION_SHARED_PREF", MODE_PRIVATE).edit();
        editor.putInt("WISSION_SHARED_PREF_LAST_POSITION", position);
        editor.apply();
    }

    public static int getLastPosition(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("WISSION_SHARED_PREF", MODE_PRIVATE);
        return prefs.getInt("WISSION_SHARED_PREF_LAST_POSITION", 0);
    }

}
