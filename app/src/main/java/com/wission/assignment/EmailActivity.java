package com.wission.assignment;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;

import com.wission.assignment.provider.WissionContract;

public class EmailActivity extends AppCompatActivity {

    TextInputLayout textInputEmail;
    Button buttonContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Cursor userCursor = getContentResolver().query(WissionContract.UserEntry.CONTENT_URI, null, WissionContract.UserEntry.COLUMN_IS_LOGGED_IN + "=?", new String[]{"1"}, null);
        if (userCursor != null) {
            if (userCursor.moveToFirst()) {
                startActivityForResult(new Intent(EmailActivity.this, FeedActivity.class), 123);
            }
            userCursor.close();
        }

        setContentView(R.layout.activity_email);

        setTitle("Login/Register");

        textInputEmail = findViewById(R.id.textInputEmail);
        buttonContinue = findViewById(R.id.buttonContinue);

        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textInputEmail.getEditText() != null) {
                    if (isValidEmail(textInputEmail.getEditText().getText().toString().trim())) {

                        //set other users to logged out
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(WissionContract.UserEntry.COLUMN_IS_LOGGED_IN, false);
                        getContentResolver().update(WissionContract.UserEntry.CONTENT_URI, contentValues, null, null);

                        //set current user to logged in
                        contentValues = new ContentValues();
                        contentValues.put(WissionContract.UserEntry.COLUMN_IS_LOGGED_IN, true);
                        int updateResult = getContentResolver().update(WissionContract.UserEntry.CONTENT_URI, contentValues, WissionContract.UserEntry.COLUMN_EMAIL + "=?", new String[]{textInputEmail.getEditText().getText().toString().trim()});

                        if (updateResult != 0) {
                            startActivity(new Intent(EmailActivity.this, FeedActivity.class));
                        } else {
                            textInputEmail.setError("");
                            Intent intent = new Intent(EmailActivity.this, RegisterActivity.class);
                            intent.putExtra(RegisterActivity.EXTRA_EMAIL, textInputEmail.getEditText().getText().toString().trim());
                            startActivity(intent);
                        }
                    } else {
                        textInputEmail.setError("Enter valid email");
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 123 && resultCode!=RESULT_OK) {
            finish();
        }
    }

    boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
