package com.wission.assignment;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wission.assignment.adapter.CommentAdapter;
import com.wission.assignment.bean.Comment;
import com.wission.assignment.provider.WissionContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FeedDetailActivity extends AppCompatActivity {
    public static String EXTRA_VIDEO_ID = "extra_video_id";
    String videoId = null;
    int userId = 0;

    ImageView imageViewVideo, imageViewLike;
    TextView textViewViewCount, textViewTitle;
    RecyclerView recyclerView;
    EditText editTextComment;
    ImageButton buttonComment;

    private CommentAdapter commentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_detail);

        imageViewVideo = findViewById(R.id.imageViewVideo);
        imageViewLike = findViewById(R.id.imageViewLike);
        textViewViewCount = findViewById(R.id.textViewViewCount);
        textViewTitle = findViewById(R.id.textViewTitle);
        recyclerView = findViewById(R.id.recyclerView);
        editTextComment = findViewById(R.id.editTextComment);
        buttonComment = findViewById(R.id.buttonComment);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (getIntent().getStringExtra(EXTRA_VIDEO_ID) != null) {
            videoId = getIntent().getStringExtra(EXTRA_VIDEO_ID);
        }

        initValues();

        imageViewLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor likeCursor = getContentResolver().query(WissionContract.LikeEntry.CONTENT_URI, null, WissionContract.LikeEntry.COLUMN_VIDEO_ID + "=? " + " and " + WissionContract.LikeEntry.COLUMN_USER_ID + "=?", new String[]{videoId, userId + ""}, null);
                if (likeCursor != null) {
                    if (likeCursor.moveToFirst()) {
                        getContentResolver().delete(WissionContract.LikeEntry.CONTENT_URI, WissionContract.LikeEntry.COLUMN_VIDEO_ID + "=? " + " and " + WissionContract.LikeEntry.COLUMN_USER_ID + "=?", new String[]{videoId, userId + ""});
                    } else {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(WissionContract.LikeEntry.COLUMN_USER_ID, userId);
                        contentValues.put(WissionContract.LikeEntry.COLUMN_VIDEO_ID, videoId);
                        getContentResolver().insert(WissionContract.LikeEntry.CONTENT_URI, contentValues);
                    }
                    initValues();
                }
            }
        });

        buttonComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextComment.getText().toString().trim().length() > 0) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(WissionContract.CommentEntry.COLUMN_USER_ID, userId);
                    contentValues.put(WissionContract.CommentEntry.COLUMN_VIDEO_ID, videoId);
                    contentValues.put(WissionContract.CommentEntry.COLUMN_COMMENT, editTextComment.getText().toString().trim());
                    getContentResolver().insert(WissionContract.CommentEntry.CONTENT_URI, contentValues);
                    initValues();
                    editTextComment.setText("");
                }
            }
        });
    }

    void initValues() {
        Cursor feedCursor = getContentResolver().query(WissionContract.FeedEntry.CONTENT_URI, null, WissionContract.FeedEntry.COLUMN_VIDEO_ID + "=?", new String[]{videoId}, null);
        if (feedCursor != null) {
            if (feedCursor.moveToFirst()) {
                Glide.with(this)
                        .load(feedCursor.getString(feedCursor.getColumnIndex(WissionContract.FeedEntry.COLUMN_THUMBNAIL)))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(imageViewVideo);
                textViewTitle.setText(feedCursor.getString(feedCursor.getColumnIndex(WissionContract.FeedEntry.COLUMN_TITLE)));
                if (feedCursor.getInt(feedCursor.getColumnIndex(WissionContract.FeedEntry.COLUMN_VIEWS)) > 0) {
                    textViewViewCount.setText(getString(R.string.text_views, feedCursor.getInt(feedCursor.getColumnIndex(WissionContract.FeedEntry.COLUMN_VIEWS))));
                } else {
                    new GetVideoViewCountAsync().execute();
                }
            }
            feedCursor.close();
        }
        Cursor userCursor = getContentResolver().query(WissionContract.UserEntry.CONTENT_URI, null, WissionContract.UserEntry.COLUMN_IS_LOGGED_IN + "=?", new String[]{"1"}, null);
        if (userCursor != null) {
            if (userCursor.moveToFirst()) {
                userId = userCursor.getInt(userCursor.getColumnIndex(WissionContract.UserEntry._ID));
            }
            userCursor.close();
        }
        Cursor likeCursor = getContentResolver().query(WissionContract.LikeEntry.CONTENT_URI, null, WissionContract.LikeEntry.COLUMN_VIDEO_ID + "=? " + " and " + WissionContract.LikeEntry.COLUMN_USER_ID + "=?", new String[]{videoId, userId + ""}, null);
        if (likeCursor != null) {
            if (likeCursor.moveToFirst()) {
                imageViewLike.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            } else {
                imageViewLike.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent));
            }
        }
        Cursor commentCursor = getContentResolver().query(WissionContract.CommentEntry.CONTENT_URI, null, WissionContract.CommentEntry.COLUMN_VIDEO_ID + "=? ", new String[]{videoId}, null);
        if (commentCursor != null) {
            List<Comment> commentList = new ArrayList<>();
            while (commentCursor.moveToNext()) {
                Comment comment = new Comment();
                comment.setComment(commentCursor.getString(commentCursor.getColumnIndex(WissionContract.CommentEntry.COLUMN_COMMENT)));
                userCursor = getContentResolver().query(WissionContract.UserEntry.CONTENT_URI, null, WissionContract.UserEntry._ID + "=?", new String[]{commentCursor.getInt(commentCursor.getColumnIndex(WissionContract.CommentEntry.COLUMN_USER_ID)) + ""}, null);
                if (userCursor != null) {
                    if (userCursor.moveToFirst()) {
                        comment.setImagePath(userCursor.getString(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_IMAGE_PATH)));
                        comment.setUserName(userCursor.getString(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_NAME)));
                    }
                    userCursor.close();
                }
                commentList.add(comment);
            }
            commentAdapter = new CommentAdapter(commentList);
            recyclerView.setAdapter(commentAdapter);
        }
    }

    private class GetVideoViewCountAsync extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                OkHttpClient client = new OkHttpClient();
                HttpUrl.Builder urlBuilder = HttpUrl.parse(getString(R.string.google_api_video_endpoint))
                        .newBuilder();

                urlBuilder.addQueryParameter("key", getString(R.string.google_api_key_wisson));
                urlBuilder.addQueryParameter("fields", "items/statistics/viewCount");
                urlBuilder.addQueryParameter("part", "statistics");
                urlBuilder.addQueryParameter("id", videoId);

                String url = urlBuilder.build().toString();
                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Response response = client.newCall(request).execute();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                String result = response.body().string();
                response.close();

                JSONObject resultObject = new JSONObject(result);
                JSONArray resultArray = resultObject.getJSONArray("items");
                int viewCount = resultArray.getJSONObject(0).getJSONObject("statistics").getInt("viewCount");
                ContentValues contentValues = new ContentValues();
                contentValues.put(WissionContract.FeedEntry.COLUMN_VIEWS, viewCount);
                getContentResolver().update(WissionContract.FeedEntry.CONTENT_URI, contentValues, WissionContract.FeedEntry.COLUMN_VIDEO_ID + "=?", new String[]{videoId});
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            initValues();
        }
    }
}
