package com.wission.assignment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.wission.assignment.provider.WissionContract;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import in.mayanknagwanshi.imagepicker.imagePicker.ImagePicker;

public class RegisterActivity extends AppCompatActivity {
    private static final int EXTERNAL_PERMISSION_CODE = 1234;
    private static final int REQUEST_CODE_FEED = 12;
    public static String EXTRA_EMAIL = "extra_email";

    String imageFilePath = null;
    boolean isEditMode = false;

    TextInputLayout textInputEmail, textInputName, textInputHandleName, textInputDob;
    ImageView imageViewEdit, imageViewDisplay;
    RadioGroup radioGroupGender;
    Button buttonContinue;

    ImagePicker imagePicker;
    final Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        imagePicker = new ImagePicker();

        textInputEmail = findViewById(R.id.textInputEmail);
        textInputName = findViewById(R.id.textInputName);
        textInputHandleName = findViewById(R.id.textInputHandleName);
        textInputDob = findViewById(R.id.textInputDob);
        imageViewEdit = findViewById(R.id.imageViewEdit);
        imageViewDisplay = findViewById(R.id.imageViewDisplay);
        buttonContinue = findViewById(R.id.buttonContinue);
        radioGroupGender = findViewById(R.id.radioGroupGender);

        if (getIntent().getStringExtra(EXTRA_EMAIL) != null) {
            setTitle("Register");
            textInputEmail.getEditText().setText(getIntent().getStringExtra(EXTRA_EMAIL));
        } else {
            setTitle("Edit Profile");
            Cursor userCursor = getContentResolver().query(WissionContract.UserEntry.CONTENT_URI, null, WissionContract.UserEntry.COLUMN_IS_LOGGED_IN + "=?", new String[]{"1"}, null);
            if (userCursor != null) {
                if (userCursor.moveToFirst()) {
                    textInputEmail.getEditText().setText(userCursor.getString(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_EMAIL)));
                    textInputName.getEditText().setText(userCursor.getString(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_NAME)));
                    textInputHandleName.getEditText().setText(userCursor.getString(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_HANDLE)));
                    textInputDob.getEditText().setText(userCursor.getString(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_DOB)));
                    imageFilePath = userCursor.getString(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_IMAGE_PATH));
                    Bitmap selectedImage = BitmapFactory.decodeFile(imageFilePath);
                    imageViewDisplay.setImageBitmap(selectedImage);
                    radioGroupGender.check(userCursor.getInt(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_IS_MALE)) == 1 ? R.id.radioButtonMale : R.id.radioButtonFemale);
                    isEditMode = true;
                }
                userCursor.close();
            }
        }


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        textInputDob.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(RegisterActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isStoragePermissionGranted()) {
                    imagePicker.withActivity(RegisterActivity.this) //calling from activity
                            .withCompression(false) //default is true
                            .start();
                } else requestStoragePermission();
            }
        });

        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textInputName.setError(null);
                textInputHandleName.setError(null);
                textInputDob.setError(null);
                textInputEmail.setError(null);

                boolean isError = false;

                if (textInputName.getEditText().getText().toString().trim().length() == 0) {
                    isError = true;
                    textInputName.setError("Enter name");
                }
                if (textInputHandleName.getEditText().getText().toString().trim().length() == 0) {
                    isError = true;
                    textInputHandleName.setError("Enter handle name");
                }
                if (textInputDob.getEditText().getText().toString().trim().length() == 0) {
                    isError = true;
                    textInputDob.setError("Enter date of birth");
                }
                if (imageFilePath == null) {
                    isError = true;
                    textInputEmail.setError("Select an image");
                }
                if (radioGroupGender.getCheckedRadioButtonId() == -1) {
                    isError = true;
                    textInputDob.setError("Select your gender");
                }
                if (!isError) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(WissionContract.UserEntry.COLUMN_EMAIL, textInputEmail.getEditText().getText().toString().trim());
                    contentValues.put(WissionContract.UserEntry.COLUMN_NAME, textInputName.getEditText().getText().toString().trim());
                    contentValues.put(WissionContract.UserEntry.COLUMN_DOB, textInputDob.getEditText().getText().toString().trim());
                    contentValues.put(WissionContract.UserEntry.COLUMN_HANDLE, textInputHandleName.getEditText().getText().toString().trim());
                    contentValues.put(WissionContract.UserEntry.COLUMN_IS_LOGGED_IN, true);
                    contentValues.put(WissionContract.UserEntry.COLUMN_IS_MALE, radioGroupGender.getCheckedRadioButtonId() == R.id.radioButtonMale);
                    contentValues.put(WissionContract.UserEntry.COLUMN_IMAGE_PATH, imageFilePath);
                    if (isEditMode) {
                        getContentResolver().update(WissionContract.UserEntry.CONTENT_URI, contentValues, WissionContract.UserEntry.COLUMN_IS_LOGGED_IN + "=?", new String[]{"1"});
                        finish();
                    } else {
                        getContentResolver().insert(WissionContract.UserEntry.CONTENT_URI, contentValues);
                        startActivityForResult(new Intent(RegisterActivity.this, FeedActivity.class), REQUEST_CODE_FEED);
                    }
                }
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        textInputDob.getEditText().setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ImagePicker.SELECT_IMAGE && resultCode == Activity.RESULT_OK) {
            String filePath = imagePicker.getImageFilePath(data);
            if (filePath != null) {//filePath will return null if compression is set to true
                imageFilePath = filePath;
                Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                imageViewDisplay.setImageBitmap(selectedImage);
            }
        } else if (requestCode == REQUEST_CODE_FEED) {
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_PERMISSION_CODE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                imagePicker.withActivity(RegisterActivity.this) //calling from activity
                        .withCompression(false) //default is true
                        .start();
            }
        }
    }

    private boolean isStoragePermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_PERMISSION_CODE);
    }
}
