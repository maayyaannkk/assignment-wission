package com.wission.assignment;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wission.assignment.adapter.FeedAdapter;
import com.wission.assignment.provider.WissionContract;
import com.wission.assignment.service.FeedIntentService;
import com.wission.assignment.util.SharedPrefUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class FeedActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    TextView textViewName, textViewHandleName, textViewAge;
    CircleImageView imageViewDisplay;
    ImageView imageViewEdit, imageViewLogout;
    RecyclerView recyclerView;

    LinearLayoutManager linearLayoutManager;
    FeedAdapter feedAdapter;

    boolean loadingMore = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        textViewName = findViewById(R.id.textViewName);
        textViewHandleName = findViewById(R.id.textViewHandleName);
        textViewAge = findViewById(R.id.textViewAge);
        imageViewDisplay = findViewById(R.id.imageViewDisplay);
        imageViewEdit = findViewById(R.id.imageViewEdit);
        imageViewLogout = findViewById(R.id.imageViewLogout);
        recyclerView = findViewById(R.id.recyclerView);

        feedAdapter = new FeedAdapter(this);
        recyclerView.setAdapter(feedAdapter);

        imageViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(WissionContract.UserEntry.COLUMN_IS_LOGGED_IN, false);
                getContentResolver().update(WissionContract.UserEntry.CONTENT_URI, contentValues, WissionContract.UserEntry.COLUMN_IS_LOGGED_IN + "=?", new String[]{"1"});
                setResult(RESULT_OK);
                finish();
            }
        });

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        int last_feed = SharedPrefUtil.getLastPosition(this);
        if (last_feed != 0) {
            linearLayoutManager.scrollToPosition(last_feed);
        }

        recyclerView.addOnScrollListener(onScrollListener);

        if (SharedPrefUtil.getNextPageToken(this) == null) {
            Toast.makeText(FeedActivity.this, "Getting Videos", Toast.LENGTH_SHORT).show();
            startService(new Intent(FeedActivity.this, FeedIntentService.class));
        }
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
            int maxPositions = layoutManager.getItemCount();

            if (lastVisibleItemPosition == maxPositions - 1) {
                if (loadingMore)
                    return;

                loadingMore = true;
                if (!SharedPrefUtil.is_feed_last) {
                    Toast.makeText(FeedActivity.this, "Getting more Videos", Toast.LENGTH_SHORT).show();
                    startService(new Intent(FeedActivity.this, FeedIntentService.class));
                } else
                    Toast.makeText(FeedActivity.this, "No More Videos", Toast.LENGTH_SHORT).show();
            } else {
                loadingMore = false;
            }
        }
    };

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return new CursorLoader(this, WissionContract.FeedEntry.CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        if (data.moveToFirst()) {
            feedAdapter.swapCursor(data);
            feedAdapter.setOnItemClickListener(new FeedAdapter.OnItemClickListener() {
                @Override
                public void onItemClicked(Cursor cursor) {
                    Intent intent = new Intent(FeedActivity.this, FeedDetailActivity.class);
                    intent.putExtra(FeedDetailActivity.EXTRA_VIDEO_ID, cursor.getString(cursor.getColumnIndex(WissionContract.FeedEntry.COLUMN_VIDEO_ID)));
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        feedAdapter.swapCursor(null);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        SharedPrefUtil.saveLastPosition(this, linearLayoutManager.findLastVisibleItemPosition());
    }

    @Override
    protected void onResume() {
        super.onResume();

        getLoaderManager().initLoader(0, null, this);

        Cursor userCursor = getContentResolver().query(WissionContract.UserEntry.CONTENT_URI, null, WissionContract.UserEntry.COLUMN_IS_LOGGED_IN + "=?", new String[]{"1"}, null);
        if (userCursor != null) {
            if (userCursor.moveToFirst()) {
                textViewName.setText(userCursor.getString(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_NAME)));
                textViewHandleName.setText(userCursor.getString(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_HANDLE)));
                textViewAge.setText(getString(R.string.text_age, dateToAge(userCursor.getString(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_DOB)))));
                Bitmap selectedImage = BitmapFactory.decodeFile(userCursor.getString(userCursor.getColumnIndex(WissionContract.UserEntry.COLUMN_IMAGE_PATH)));
                imageViewDisplay.setImageBitmap(selectedImage);

                imageViewEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(FeedActivity.this, RegisterActivity.class);
                        startActivity(intent);
                    }
                });

            }
            userCursor.close();
        }
    }

    private int dateToAge(String dobString) {
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        return age;
    }
}
